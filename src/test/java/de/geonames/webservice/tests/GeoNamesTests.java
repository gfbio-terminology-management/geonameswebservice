package de.geonames.webservice.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonObject;
import org.junit.Test;
import de.geonames.webservice.GeonamesWSServiceImpl;

public class GeoNamesTests {

  @Test
  public void testSupportedMatchTypes() {
    GeonamesWSServiceImpl ws = new GeonamesWSServiceImpl();
    assertTrue(ws.supportsMatchType("exact"));
    assertTrue(ws.supportsMatchType("included"));
  }

  @Test
  public void WSSynonymsTest1() {
    System.out.println("*** Test Synonyms ***");
    GeonamesWSServiceImpl ws = new GeonamesWSServiceImpl();
    ArrayList<JsonObject> result = ws.getSynonyms("2950159").create(); // 2950159
                                                                       // =
                                                                       // Berlin
    assertNotNull(result);
    System.out.println(result);
  }

  @Test
  public void WSSynonymsTest2() {
    System.out.println("*** Test Synonyms 2 ***");
    GeonamesWSServiceImpl ws = new GeonamesWSServiceImpl();
    ArrayList<JsonObject> result = ws.getSynonyms("http://www.geonames.org/2950159/").create();
    assertNotNull(result);
    for (JsonObject a : result) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSSearchTestIncluded() {
    System.out.println("*** Test Search Included ***");
    GeonamesWSServiceImpl ws = new GeonamesWSServiceImpl();
    List<JsonObject> results = ws.search("Berli", "included").create();
    System.out.println(results);
  }

  @Test
  public void WSSearchTestExact() {
    System.out.println("*** Test Search Exact ***");
    GeonamesWSServiceImpl ws = new GeonamesWSServiceImpl();
    List<JsonObject> results = ws.search("Berlin", "exact").create();
    assertFalse(results.isEmpty());
    for (JsonObject result : results) {
      System.out.println(result);
    }
  }

  @Test
  public void WSSearchTestHierarchy() {
    System.out.println("*** Test hierarchy ***");
    GeonamesWSServiceImpl ws = new GeonamesWSServiceImpl();
    List<JsonObject> results = ws.getHierarchy("2950159").create();
    String BerlinHierarchy[] = {"Berlin", "Berlin", "Berlin, Stadt", "Land Berlin", "Germany",
        "Europe", "Earth", "context"};
    assertTrue(results.size() == BerlinHierarchy.length);
    for (int i = 0; i < BerlinHierarchy.length; i++) {
      assertTrue(results.get(i).toString().contains(BerlinHierarchy[i]));
    }
    System.out.println(results);
  }

  @Test
  public void WSSearchTestAllBroader() {

    System.out.println("*** Test allbroader ***");
    GeonamesWSServiceImpl ws = new GeonamesWSServiceImpl();
    List<JsonObject> results = ws.getAllBroader("2950159").create();
    String BerlinHierarchy[] =
        {"Earth", "Europe", "Germany", "Land Berlin", "Berlin, Stadt", "Berlin", "context"};
    assertTrue(results.size() == BerlinHierarchy.length);
    for (int i = 0; i < BerlinHierarchy.length; i++) {

      assertTrue(results.get(i).toString().contains(BerlinHierarchy[i]));

    }
    for (JsonObject a : results) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermCombined() {

    System.out.println("*** Test termCombined ***");
    GeonamesWSServiceImpl serviceIMPL = new GeonamesWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getTermInfosCombined("965039").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermProcessed() {
    System.out.println("*** Test term processed ***");
    GeonamesWSServiceImpl serviceIMPL = new GeonamesWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getTermInfosProcessed("965039").create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSmetadata() {
    System.out.println("*** Test metadata service ***");
    GeonamesWSServiceImpl serviceIMPL = new GeonamesWSServiceImpl();
    ArrayList<JsonObject> check = serviceIMPL.getMetadata().create();
    assertNotNull(check);
    for (JsonObject a : check) {
      System.out.println(a.toString());
    }
  }
}
