package de.geonames.webservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.geonames.InsufficientStyleException;
import org.geonames.Style;
import org.geonames.Toponym;
import org.geonames.ToponymSearchCriteria;
import org.geonames.WebService;
import org.gfbio.config.WSConfiguration;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.util.YAMLConfigReader;

/**
 * Webservice implementation in the GFBio Terminology Service. This class implements all necessary
 * methods for the TS endpoints according to the WSInterface defined in the gfbioapi project.
 * 
 * @author nkaram <A HREF="mailto:naouel.karam@fu-berlin.de">Naouel Karam</A>
 * 
 */
public class GeonamesWSServiceImpl implements WSInterface {
  private static final Logger LOGGER = Logger.getLogger(GeonamesWSServiceImpl.class);
  private final String userName = "alexh";

  // properties from YAML file
  private WSConfiguration wsConfig;

  public GeonamesWSServiceImpl() {

    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    wsConfig = reader.getWSConfig("GEONAMES");

    WebService.setUserName(this.userName);

    LOGGER.info("GEONAMES webservice is ready to use");
  }


  public String getAcronym() {
    return wsConfig.getAcronym();
  }

  public boolean supportsMatchType(String matchType) {
    return Arrays.stream(wsConfig.getSearchModes())
        .anyMatch(SearchModes.valueOf(matchType)::equals);
  }

  public String getDescription() {
    return wsConfig.getDescription();
  }

  public String getName() {
    return wsConfig.getName();
  }

  public String getURI() {
    return wsConfig.getWebserviceURL();
  }

  /**
   * Implements the getTermInfosOriginal method of the WSInterface Gets the informations of a taxon
   * given its URI with the original attributes
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String termUri) {
    int id = extractId(termUri);
    if (id == -1) {
      return null;
    }
    LOGGER.info("term query " + termUri);
    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("geonames");
    try {
      Toponym result = WebService.get(id, "en", "FULL");
      TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();
      e.setOriginalTermInfo("name", result.getName());
      e.setOriginalTermInfo("fcode", result.getFeatureCode());
      e.setOriginalTermInfo("GeoNameId", Integer.toString(result.getGeoNameId()));
      ArrayList<String> alt = new ArrayList<String>();
      for (String n : result.getAlternateNames().split(",")) {
        if (n.trim() != "") {
          alt.add(n);
        }
      }
      if (!alt.isEmpty()) {
        e.setOriginalTermInfo("alternateNames", alt);
      }
      if (result.getElevation() != null) {
        e.setOriginalTermInfo("elevation", Integer.toString(result.getElevation()));
      }
      e.setOriginalTermInfo("featureClass", result.getFeatureClass().name());
      e.setOriginalTermInfo("featureName", result.getFeatureCodeName());
      if (result.getPopulation() != null) {
        e.setOriginalTermInfo("population", result.getPopulation().toString());
      }
      e.setOriginalTermInfo("latitude", Double.toString(result.getLatitude()));
      e.setOriginalTermInfo("longitude", Double.toString(result.getLongitude()));
      rs.addEntry(e);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return rs;
  }

  /**
   * Implements the getTermInfosProcessed method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String termUri) {
    int id = extractId(termUri);
    if (id == -1) {
      return null;
    }
    LOGGER.info("term query " + termUri);
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("geonames");
    try {
      Toponym result = WebService.get(id, "en", "FULL");
      SearchResultSetEntry e = new SearchResultSetEntry();
      e.setLabel(result.getName());
      e.setRank(result.getFeatureCode());
      e.setExternalID(Integer.toString(result.getGeoNameId()));
      e.setUri(wsConfig.getWebserviceURL() + String.valueOf(result.getGeoNameId()) + "/");
      ArrayList<String> alt = new ArrayList<String>();
      for (String n : result.getAlternateNames().split(",")) {
        if (n.trim() != "") {
          alt.add(n);
        }
      }
      if (!alt.isEmpty()) {
        e.setCommonNames(alt);
      }
      e.setSourceTerminology(getAcronym());
      rs.addEntry(e);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return rs;
  }

  /**
   * Implements the getTermInfosCombined method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes when mapped and original attributes when not
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String externalId) {
    int id = extractId(externalId);
    if (id == -1) {
      return null;
    }
    LOGGER.info("term query " + externalId);
    GFBioResultSet<TermCombinedResultSetEntry> rs =
        new GFBioResultSet<TermCombinedResultSetEntry>("geonames");
    try {
      Toponym result = WebService.get(id, "en", "FULL");
      TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();
      e.setLabel(result.getName());
      e.setRank(result.getFeatureCode());
      e.setExternalID(Integer.toString(result.getGeoNameId()));
      e.setUri(wsConfig.getWebserviceURL() + String.valueOf(result.getGeoNameId() + "/"));
      ArrayList<String> alt = new ArrayList<String>();
      for (String n : result.getAlternateNames().split(",")) {
        if (n.trim() != "") {
          alt.add(n);
        }
      }
      if (!alt.isEmpty()) {
        e.setCommonNames(alt);
      }
      e.setSourceTerminology(getAcronym());
      if (result.getElevation() != null) {
        e.setOriginalTermInfo("elevation", Integer.toString(result.getElevation()));
      }
      e.setOriginalTermInfo("featureClass", result.getFeatureClass().name());
      e.setOriginalTermInfo("featureName", result.getFeatureCodeName());
      if (result.getPopulation() != null) {
        e.setOriginalTermInfo("population", result.getPopulation().toString());
      }
      e.setOriginalTermInfo("latitude", Double.toString(result.getLatitude()));
      e.setOriginalTermInfo("longitude", Double.toString(result.getLongitude()));
      rs.addEntry(e);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return rs;
  }

  public boolean isResponding() {
    return true;
  }


  /** missing java doc comments for all functions. **/
  public List<String> getDomains() {
    List<String> l = new ArrayList<String>();
    for (Domains d : wsConfig.getWsDomains()) {
      l.add(d.name());
    }
    return l;
  }

  /**
   * Implements the getAllBroader method of the WSInterface Gets all broader terms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String externalId) {
    int id = extractId(externalId);
    if (id == -1) {
      return null;
    }
    GFBioResultSet<AllBroaderResultSetEntry> rs =
        new GFBioResultSet<AllBroaderResultSetEntry>("geonames");
    try {
      List<Toponym> results = WebService.hierarchy(id, "en", Style.FULL);
      for (int i = 0; i < results.size() - 1; i++) {
        Toponym result = results.get(i);
        AllBroaderResultSetEntry e = new AllBroaderResultSetEntry();
        e.setRank(String.valueOf(result.getFeatureCode()));
        e.setLabel(String.valueOf(result.getName()));
        e.setUri(wsConfig.getWebserviceURL() + String.valueOf(result.getGeoNameId()) + "/");
        e.setExternalID(Integer.toString(result.getGeoNameId()));
        rs.addEntry(e);
      }
    } catch (Exception e) {
      LOGGER.warn("Exception: " + e.getMessage());
      e.printStackTrace();
    }
    return rs;
  }

  /**
   * Implements the getHierarchy method of the WSInterface Gets the broader hierarchy of a taxon
   * given its URI or External ID
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String externalId) {
    int id = extractId(externalId);
    if (id == -1) {
      return null;
    }
    GFBioResultSet<HierarchyResultSetEntry> rs =
        new GFBioResultSet<HierarchyResultSetEntry>("geonames");
    try {
      List<Toponym> results = WebService.hierarchy(id, "en", Style.FULL);
      for (int i = results.size() - 1; i > 0; i--) {
        Toponym current = results.get(i);
        Toponym father = results.get(i - 1);

        HierarchyResultSetEntry e = new HierarchyResultSetEntry();
        e.setLabel(current.getName());
        e.setUri(wsConfig.getWebserviceURL() + String.valueOf(current.getGeoNameId()) + "/");
        e.setRank(current.getFeatureCode());
        ArrayList<String> arr = new ArrayList<String>();
        arr.add(wsConfig.getWebserviceURL() + father.getGeoNameId());
        e.setHierarchy(arr);
        rs.addEntry(e);
      }

      Toponym topResult = results.get(0);
      HierarchyResultSetEntry topEntry = new HierarchyResultSetEntry();
      topEntry.setLabel(topResult.getName());
      topEntry.setUri(wsConfig.getWebserviceURL() + String.valueOf(topResult.getGeoNameId()) + "/");
      topEntry.setRank(topResult.getFeatureCode());
      topEntry.setHierarchy(new ArrayList<String>());
      rs.addEntry(topEntry);
    } catch (Exception e) {
      LOGGER.warn("Exception: " + e.getMessage());
      e.printStackTrace();
    }
    return rs;
  }

  private int extractId(String param) {
    if (param.matches("\\d+")) {
      return Integer.valueOf(param);
    }
    Pattern pattern = Pattern.compile("geonames.org/(\\d+)");
    Matcher matcher = pattern.matcher(param);
    if (matcher.find()) {
      return Integer.valueOf(matcher.group(1));
    }
    return -1;
  }

  /**
   * Implements the getSynonyms method of the WSInterface Gets the list of synonyms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<SynonymsResultSetEntry> getSynonyms(String externalId) {
    LOGGER.info("synonym query " + externalId);
    GFBioResultSet<SynonymsResultSetEntry> rs =
        new GFBioResultSet<SynonymsResultSetEntry>("geonames");
    int id = extractId(externalId);
    if (id == -1) {
      return null;
    }
    try {
      Toponym result = WebService.get(id, "en", "FULL");
      SynonymsResultSetEntry e = new SynonymsResultSetEntry();
      e.setSynonyms(this.getAlternateNames(result));
      rs.addEntry(e);
      return rs;
    } catch (Exception e) {
      LOGGER.warn("Exception: " + e.getMessage());
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Implements the search method of the WSInterface Searches for names and common names in the
   * database based on the match type "exact" or "included"
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<SearchResultSetEntry> search(String query, String matchType) {
    LOGGER.info("search query " + query + " matchtype " + matchType);
    ToponymSearchCriteria searchCriteria = new ToponymSearchCriteria();
    if (matchType.equals(SearchTypes.exact.name())) {
      searchCriteria.setNameEquals(query);
    } else {
      searchCriteria.setName(query);
    }
    // Ask for long search results to get alternate names
    searchCriteria.setStyle(Style.FULL);
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("geonames");
    try {
      List<Toponym> searchResults = WebService.search(searchCriteria).getToponyms();
      for (Toponym result : searchResults) {
        if (matchType.equals(SearchTypes.exact.name())) {
          if (!result.getName().toLowerCase().equals(query.toLowerCase())) {
            continue;
          }
        }
        SearchResultSetEntry e = new SearchResultSetEntry();
        e.setLabel(result.getName());
        e.setRank(result.getFeatureCode());
        e.setExternalID(Integer.toString(result.getGeoNameId()));
        e.setUri(wsConfig.getWebserviceURL() + String.valueOf(result.getGeoNameId()) + "/");
        ArrayList<String> alt = new ArrayList<String>();
        for (String n : result.getAlternateNames().split(",")) {
          if (n.trim() != "") {
            alt.add(n);
          }
        }
        if (!alt.isEmpty()) {
          e.setCommonNames(alt);
        }
        e.setSourceTerminology(getAcronym());
        rs.addEntry(e);
      }
    } catch (Exception e) {
      LOGGER.warn("Exception: " + e.getMessage());
      e.printStackTrace();
    }
    return rs;
  }

  private ArrayList<String> getAlternateNames(Toponym result) {
    ArrayList<String> alternateNames = new ArrayList<String>();
    try {
      for (String name : result.getAlternateNames().split(",")) {
        if (!alternateNames.contains(name)) {
          alternateNames.add(name);
        }
      }
    } catch (InsufficientStyleException e) {
      e.printStackTrace();
    }
    return alternateNames;
  }

  /**
   * Implements the getMetadata method of the WSInterface Gets the metadata information about the
   * webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<MetadataResultSetEntry> getMetadata() {
    MetadataResultSetEntry e = new MetadataResultSetEntry();
    e.setName(getName());
    e.setAcronym(getAcronym());
    // e.setVersion(version);
    e.setDescription(getDescription());
    // e.setKeywords(keywords);
    e.setUri(getURI());
    e.setContact(wsConfig.getContact());
    e.setContribution(wsConfig.getContribution());
    e.setStorage(wsConfig.getStorage());
    ArrayList<String> namespaces = new ArrayList<String>();
    namespaces.add(wsConfig.getWebserviceURL());
    e.setNamespaces(namespaces);
    ArrayList<String> domainUris = new ArrayList<String>();
    for (Domains d : wsConfig.getWsDomains()) {
      domainUris.add(d.getUri());
    }
    e.setDomain(domainUris);
    GFBioResultSet<MetadataResultSetEntry> rs =
        new GFBioResultSet<MetadataResultSetEntry>("geonames");
    rs.addEntry(e);
    return rs;
  }

  /**
   * Implements the getCapabilities method of the WSInterface Returns the available service
   * endpoints for the webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities() {
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();
    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : wsConfig.getAvailableServices()) {
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : wsConfig.getSearchModes()) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>("geonames");
    rs.addEntry(e);
    return rs;
  }


  @Override
  public String getCurrentVersion() {
    return "VERSION TAG";
  }

}
